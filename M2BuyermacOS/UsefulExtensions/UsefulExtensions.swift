//
//  UsefulExtensions.swift
//  M2BuyermacOS
//
//  Created by cedcoss on 26/03/21.
//

import Foundation

extension Dictionary{
    func convtToJson() -> NSString {
        do {
            let json = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return NSString(data: json, encoding: String.Encoding.utf8.rawValue)!
        }catch {
            return ""
        }
    }
}
