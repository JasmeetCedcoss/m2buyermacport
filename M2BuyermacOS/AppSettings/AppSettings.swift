//
//  AppSettings.swift
//  M2BuyermacOS
//
//  Created by cedcoss on 25/03/21.
//

import Foundation

struct Settings{
    
    static var messageTopic = "magenativeios";
    static var baseUrl      = "https://demo.cedcommerce.com/magento4/vrushank/rest/V1/";
    static var sellerUrl    = "http://demo.cedcommerce.com/magento4/vrushank/";
    static var headerKey    = "hashkey:3xNOO2C66mmJrJqV4R5xKVDskCQ0gu79f";
//    static var themeColor: UIColor{
//        if #available(iOS 13.0, *) {
//
//          return UIColor { (UITraitCollection) -> UIColor in
//            if UITraitCollection.userInterfaceStyle == .dark { return UIColor(hexString: "#001A27")! }
//            else { return UIColor(hexString: "#001A27")! }
//          }
//        } else {
//            return UIColor(hexString: "#001A27")!
//        }
//    } //= "#001A27";
//    static var themeTextColor: UIColor{
//        if #available(iOS 13.0, *) {
//
//          return UIColor { (UITraitCollection) -> UIColor in
//            if UITraitCollection.userInterfaceStyle == .dark { return UIColor(hexString: "#FFFFFF")! }
//            else { return UIColor(hexString: "#FFFFFF")! }
//          }
//        } else {
//            return UIColor(hexString: "#FFFFFF")!
//        }
//    } //= "#001A27";
    static var textColor = "#000000";
    static var debugMode = true;
    static var layoutDebug = true;
}
