//
//  MenuListingCVCell.swift
//  M2BuyermacOS
//
//  Created by cedcoss on 12/04/21.
//

import Cocoa

class MenuListingCVCell: NSCollectionViewItem {
    
    @IBOutlet weak var menuTitle: NSTextField!
    @IBOutlet weak var menuImage: NSImageView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    var data: String?{
        didSet{
            menuTitle.stringValue = data ?? ""
        }
    }
}
