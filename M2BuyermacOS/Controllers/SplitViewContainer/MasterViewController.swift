//
//  MasterViewController.swift
//  M2BuyermacOS
//
//  Created by cedcoss on 02/04/21.
//

import Cocoa

class MasterViewController: NSViewController {

    @IBOutlet weak var collectionView: NSCollectionView!
    
    let demoArr = ["Menu A","Menu B","Menu C","Menu D","Menu E"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        collectionView.dataSource = self
        collectionView.delegate   = self
        let nib = NSNib(nibNamed: NSUserInterfaceItemIdentifier(rawValue: "MenuListingCVCell").rawValue, bundle: nil)
        collectionView.register(nib, forItemWithIdentifier: NSUserInterfaceItemIdentifier(rawValue: "MenuListingCVCell"))
        
    }
}

extension MasterViewController: NSCollectionViewDataSource{
    
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return demoArr.count
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier("MenuListingCVCell"), for: indexPath)
        guard let collectionViewItem = item as? MenuListingCVCell else {return item}
        collectionViewItem.data      = demoArr[indexPath.item]
        return item
      }
    }

extension MasterViewController: NSCollectionViewDelegate,NSCollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> NSSize {
        return NSSize(width: collectionView.frame.width, height: 50)
    }
}
