//
//  CoreDataManager.swift
//  M2BuyermacOS
//
//  Created by cedcoss on 02/04/21.
//

import Foundation
import CoreData
import Cocoa
import SwiftyJSON

final class CoreDataManager{
    
    static let shared  = CoreDataManager()
    //  MARK: - Save latest token
    //
    func saveCartData(_ cartJson: JSON) {
        CoreDataManager().deleteOldCartData()
        let json                        = cartJson[0]
        let AppInformation              = GetCartData(context: PersistenceService.context)
        if json["items_count"].stringValue == "" {
            AppInformation.itemCount    = "0"
        }else{
            AppInformation.itemCount    = json["items_count"].stringValue
        }
        AppInformation.checkoutEnabled  = json["webview_checkout"].stringValue
        AppInformation.gender           = json["gender"].stringValue
        AppInformation.name             = json["name"].stringValue
        AppInformation.storeID          = json["default_store"].stringValue
        PersistenceService.saveContext()
        CoreDataManager().showCurrentCartData()
    }

    
    //  MARK: - Delete old cartData from Core Database
    //
    func deleteOldCartData(){
        let demo =  try! PersistenceService.context.fetch(GetCartData.fetchRequest()) as? [GetCartData]
        demo?.forEach({
            PersistenceService.context.delete($0)
        })
    }
    
    //  MARK: - Show current cartData
    //
    func showCurrentCartData(){
        guard let demo =  try! PersistenceService.context.fetch(GetCartData.fetchRequest()) as? [GetCartData] else{ return Logger.log(.warning, "Cannot Access")}
        if !demo.isEmpty{
            demo.forEach({
                Logger.log(.success, "CurrentCartDataIs==\n\($0.debugDescription)")
            })
        }else{
            Logger.log(.warning,"CurrentCartDataIsEmpty")
        }
    }
}


