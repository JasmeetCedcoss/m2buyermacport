//
//  GetCartData+CoreDataProperties.swift
//  M2BuyermacOS
//
//  Created by cedcoss on 02/04/21.
//
//

import Foundation
import CoreData

extension GetCartData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GetCartData> {
        return NSFetchRequest<GetCartData>(entityName: "GetCartData")
    }

    @NSManaged public var itemCount: String
    @NSManaged public var checkoutEnabled: String
    @NSManaged public var gender: String
    @NSManaged public var name: String
    @NSManaged public var storeID: String

}
