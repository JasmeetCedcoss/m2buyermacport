//
//  NetworkManager.swift
//  M2BuyermacOS
//
//  Created by cedcoss on 26/03/21.
//

import Foundation

class NetworkManager {

    static let shared = NetworkManager()
    private init() {}
    
    enum RequestType {
        case GET
        case POST
    }

    func sendrequestForData(with url: String, params: [String:String]?=nil, requestType: RequestType, /*controller:  NSViewController,*/ showLoader:Bool = true, completion:(@escaping (_ json: Data?,_ error: Error?, _ response: URLResponse?)-> Void)) {

       // if showLoader { dailyboxLoader.shared.showLoader() }

        DispatchQueue.global(qos: .background).async {

            var baseString  = String()
            baseString      = Settings.baseUrl

            let endPoint        = baseString + url
            guard let finalUrl  = URL(string: endPoint) else {return}

            var stringToPost = [String:[String:String]]()
            var postJsonString  = ""

            var request = URLRequest(url: finalUrl)

            // Checking for Request Type

            switch requestType {
            case .GET:
                request.httpMethod    = "GET"
            default:
                request.httpMethod    = "POST"
                if let param          = params {

                    stringToPost        = ["parameters":[:]]
                    for (key,value) in param
                    {
                        _ = stringToPost["parameters"]?.updateValue(value, forKey:key)
                    }
                    postJsonString      = stringToPost.convtToJson() as String
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                    request.httpBody = postJsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                }
            }

            print(finalUrl)
            print(postJsonString)

            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
                    print("status code should be 200 but is \(httpResponse.statusCode)")
                    print("Bad response is \(httpResponse)")
                    DispatchQueue.main.async {
                       // dailyboxLoader.shared.hideLoader()
                    }
                   //// completion(error)
                }
                guard error == nil && data != nil else {
                    DispatchQueue.main.async {
                        if showLoader {/* dailyboxLoader.shared.hideLoader()*/}
//                        controller.view.makeToast("Error is " + error!.localizedDescription, duration: 2.0, position: .center)
                    }
                    return
                }
                DispatchQueue.main.async
                    {
                       // if showLoader { dailyboxLoader.shared.hideLoader()}
                        completion(data,error,response)
                }
            }.resume()
        }
    }
}
