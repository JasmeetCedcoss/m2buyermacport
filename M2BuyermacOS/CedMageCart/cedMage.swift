//
//  cedMage.swift
//  M2BuyermacOS
//
//  Created by cedcoss on 25/03/21.
//

import Foundation
import SwiftyJSON

class cedMage : NSObject {
    
    //MARK: Get Cart Count
    func getCartCount(){
        
        let url = Settings.baseUrl;
        let getAccount  = url + "mobiconnect/checkout/getcartcount"
        let cartReq     = NSMutableURLRequest(url: URL(string: getAccount)!)/*URLRequest(url: URL(string: getAccount)! )*/
        let defaults    = UserDefaults.standard
        var cartId      = String()
        var postString  = "";
        var postData    = [String:String]()
        
        postData["store_id"]    = "1"
        postData["app_version"] = "1.8"
        
//        if(!defaults.bool(forKey: "isLogin")){
//            let v = defaults.object(forKey: "cartId") as? String
//            if(v != nil){
//                cartId = v!
//                postData["cart_id"] = cartId;
//            }else{
//                postData["cart_id"] = "0"
//            }
//        }
//        else if(defaults.bool(forKey: "isLogin")){
//            let userInfoDict    = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
//            print(userInfoDict["hashKey"] as Any)
//            let hashKey             = userInfoDict["hashKey"]
//            let customerId          = userInfoDict["customerId"]
//            postData["hashkey"]     = hashKey;
//            postData["customer_id"] = customerId;
//
//            if(defaults.object(forKey: "userCartId") != nil)
//            {
//                let cart_id = defaults.object(forKey: "userCartId") as? String;
//                postData["cart_id"] = cart_id;
//            }else{
//                postData["cart_id"] = "0"
//            }
//        }
        
        
        postString = ["parameters":postData].convtToJson() as String
        print(postString)
        
        cartReq.httpMethod  = "POST"
        cartReq.httpBody    = postString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue));
        let requestHeader   = Settings.headerKey
        
        cartReq.setValue("application/json", forHTTPHeaderField: "Content-Type")
        cartReq.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                cartReq.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
        }
        
        print(postString)
        print(getAccount)
        //var error: NSError?
        let session = URLSession.shared
        
        do {

            var task = session.dataTask(with: cartReq as URLRequest, completionHandler: {data, response, error -> Void in
                    print("Response: \(response)")
                
                let httpResponse = response as? HTTPURLResponse
                if(httpResponse?.statusCode != 200)
                {
                    DispatchQueue.main.async {
                        
                    }
                }
                guard let json = try? JSON(data: data ?? Data()) else{return;}
                print("json----\(json)")
                
                if(json[0]["success"].stringValue == "true"){
                    CoreDataManager().saveCartData(json)
                }
            })
            task.resume()
            
//            if #available(iOS 9, *){
//
//                let data = try NSURLConnection.sendSynchronousRequest(cartReq, returning: &response)
//                //checking for network error
//                let httpResponse = response as! HTTPURLResponse
//                // print(httpResponse)
//                if(httpResponse.statusCode != 200)
//                {
//                    DispatchQueue.main.async {
//
//                    }
//                }
//                // code to fetch values from response :: start
//                guard let json = try? JSON(data: data) else{return;}
//                print("json----\(json)")
//                if(json[0]["success"].stringValue == "true"){
//                    if(json[0]["items_count"].stringValue == ""){
//                        defaults.set("0", forKey: "items_count")
//                    }else{
//                        defaults.set(json[0]["items_count"].stringValue, forKey: "items_count")
//                    }
//                    defaults.setValue(json[0]["gender"].stringValue, forKey: "gender")
//                    defaults.setValue(json[0]["name"].stringValue, forKey: "name")
//                    if (defaults.value(forKey: "storeId") == nil){
//                        defaults.setValue(json[0]["default_store"].stringValue, forKey: "storeId")
//                    }
//                    defaults.setValue(json[0]["webview_checkout"].stringValue, forKey: "checkoutEnable")
//                    if (UserDefaults.standard.value(forKey: "mageAppLang") == nil){
//                        let lang=json[0]["locale"].stringValue
//                        let language=lang.components(separatedBy: "_")
//
//                        if language[0]=="ar"
//                        {
//                            UserDefaults.standard.set(["ar","en","fr"], forKey: "mageAppLang")
//                        }
//                        else
//                        {
//                            UserDefaults.standard.set([language[0],"ar","fr"], forKey: "mageAppLang")
//                        }
//                    }
//
////                    if let locale = UserDefaults.standard.value(forKey: "mageAppLang") as? [String] {
////                        if locale[0]=="ar"
////                        {
////                            UIView.appearance().semanticContentAttribute = .forceRightToLeft
////                        }
////                        else
////                        {
////                            UIView.appearance().semanticContentAttribute = .forceLeftToRight
////                        }
////                    }
//
//                    //load_app()
//                }
//            }
            
        }catch {
            print("Error")
        }
    }
}
